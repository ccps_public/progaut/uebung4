#ifndef UEBUNG4_INTEGRATE_H
#define UEBUNG4_INTEGRATE_H

#include <cstddef>

double integrate(double (*fct)(double), double x_start, double x_end, size_t n_steps);

double f1(double x);
double f2(double x);

#endif  // UEBUNG4_INTEGRATE_H
