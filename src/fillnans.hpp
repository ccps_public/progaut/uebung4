#ifndef UEBUNG4_FILLNANS_H
#define UEBUNG4_FILLNANS_H

#include<cstddef>

enum class InterpMethod {
    Left,
    Linear,
};

size_t fill_nans(double* data, size_t len, InterpMethod method);

#endif  // UEBUNG4_FILLNANS_H
