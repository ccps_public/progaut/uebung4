#include "integrate.hpp"

#include <gtest/gtest.h>

TEST(function, one) { EXPECT_NEAR(f1(0.4), 0.56, 0.01); }

TEST(function, two) { EXPECT_NEAR(f2(2), 0.693, 0.01); }

static double one(double) { return 1.0; }

TEST(integrate, constant) {
    EXPECT_NEAR(integrate(one, 0, 10, 1), 10.0, 1e-12);
    EXPECT_NEAR(integrate(one, 1, 10, 2), 9.0, 1e-12);
    EXPECT_NEAR(integrate(one, 2, 10, 10000), 8.0, 1e-12);
}

TEST(integrate, functions) {
    EXPECT_NEAR(integrate(f1, 0, 10, 100000), 383.33, 0.02);
    EXPECT_NEAR(integrate(f2, 1, 10, 100000), 14.0373, 0.02);
}
