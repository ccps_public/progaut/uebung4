#ifndef UEBUNG4_STRING_TOOLS_H
#define UEBUNG4_STRING_TOOLS_H

#include <string>

bool isWordPalindrom(const std::string& word);

void concat_s(char* target, size_t target_size, const char* string1, const char* string2);

#endif  // UEBUNG4_STRING_TOOLS_H
