#include "string_tools.hpp"

#include <gtest/gtest.h>

#include <cstdio>

TEST(isWordPalindrom, test) {
    EXPECT_TRUE(isWordPalindrom(""));
    EXPECT_TRUE(isWordPalindrom("a"));
    EXPECT_TRUE(isWordPalindrom("anna"));
    EXPECT_TRUE(isWordPalindrom("Anna"));
    EXPECT_FALSE(isWordPalindrom("Tomate"));
}

constexpr char UNWRITTEN = (char)0xFF;

static void init_buffer(char* buf, size_t bufsize) {
    for (size_t i = 0; i < bufsize; ++i) {
        *buf++ = UNWRITTEN;
    }
}

TEST(concat_s, test) {
    // use one byte before and two bytes after buffer that is given to the
    // function to assert that the function doesn't touch the surounding bytes.
    constexpr size_t TRUEBUFLEN = 13;
    char true_buf[TRUEBUFLEN];

    constexpr size_t BUFLEN = 10;
    char* buf = true_buf + 1;

    init_buffer(true_buf, TRUEBUFLEN);
    concat_s(buf, BUFLEN, "Test", "that");
    EXPECT_EQ(strcmp(buf, "Testthat"), 0);
    EXPECT_EQ(buf[-1], UNWRITTEN);
    EXPECT_EQ(buf[BUFLEN], UNWRITTEN);
    EXPECT_EQ(buf[BUFLEN + 1], UNWRITTEN);

    init_buffer(true_buf, TRUEBUFLEN);
    concat_s(buf, BUFLEN, " 0Test", " 2 ");
    EXPECT_EQ(strcmp(buf, " 0Test 2 "), 0);
    EXPECT_EQ(buf[-1], UNWRITTEN);
    EXPECT_EQ(buf[BUFLEN], UNWRITTEN);
    EXPECT_EQ(buf[BUFLEN + 1], UNWRITTEN);

    init_buffer(true_buf, TRUEBUFLEN);
    concat_s(buf, BUFLEN, "1234567890123", "ABC");
    EXPECT_EQ(strcmp(buf, "123456789"), 0);
    EXPECT_EQ(buf[-1], UNWRITTEN);
    EXPECT_EQ(buf[BUFLEN], UNWRITTEN);
    EXPECT_EQ(buf[BUFLEN + 1], UNWRITTEN);

    init_buffer(true_buf, TRUEBUFLEN);
    concat_s(buf, BUFLEN, "ABC", "1234567890123");
    EXPECT_EQ(strcmp(buf, "ABC123456"), 0);
    EXPECT_EQ(buf[-1], UNWRITTEN);
    EXPECT_EQ(buf[BUFLEN], UNWRITTEN);
    EXPECT_EQ(buf[BUFLEN + 1], UNWRITTEN);

    init_buffer(true_buf, TRUEBUFLEN);
    concat_s(buf, BUFLEN, "123456789", "ABC");
    EXPECT_EQ(strcmp(buf, "123456789"), 0);
    EXPECT_EQ(buf[-1], UNWRITTEN);
    EXPECT_EQ(buf[BUFLEN], UNWRITTEN);
    EXPECT_EQ(buf[BUFLEN + 1], UNWRITTEN);

    init_buffer(true_buf, TRUEBUFLEN);
    concat_s(buf, BUFLEN, "xy", "");
    EXPECT_EQ(strcmp(buf, "xy"), 0);
    EXPECT_EQ(buf[-1], UNWRITTEN);
    EXPECT_EQ(buf[BUFLEN], UNWRITTEN);
    EXPECT_EQ(buf[BUFLEN + 1], UNWRITTEN);

    init_buffer(true_buf, TRUEBUFLEN);
    concat_s(buf, BUFLEN, "", "ab");
    EXPECT_EQ(strcmp(buf, "ab"), 0);
    EXPECT_EQ(buf[-1], UNWRITTEN);
    EXPECT_EQ(buf[BUFLEN], UNWRITTEN);
    EXPECT_EQ(buf[BUFLEN + 1], UNWRITTEN);

    init_buffer(true_buf, TRUEBUFLEN);
    concat_s(buf, BUFLEN, "", "");
    EXPECT_EQ(strcmp(buf, ""), 0);
    EXPECT_EQ(buf[-1], UNWRITTEN);
    EXPECT_EQ(buf[BUFLEN], UNWRITTEN);
    EXPECT_EQ(buf[BUFLEN + 1], UNWRITTEN);

    init_buffer(true_buf, TRUEBUFLEN);
    concat_s(buf, 1, "a", "b");
    EXPECT_EQ(strcmp(buf, ""), 0);
    EXPECT_EQ(buf[-1], UNWRITTEN);
    EXPECT_EQ(buf[1], UNWRITTEN);
    EXPECT_EQ(buf[2], UNWRITTEN);
}
