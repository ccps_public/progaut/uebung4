#include "fillnans.hpp"

#include <gtest/gtest.h>

#include <cmath>
#include <vector>

static void check_array(double* data, std::vector<double> ref,
                        double tol = 1e-12) {
    for (size_t i = 0; i < ref.size(); ++i) {
        EXPECT_NEAR(data[i], ref[i], tol);
    }
}

TEST(fillnans, left) {
    double data1[]{1, 2, 3, 4};
    const size_t count1 =
        fill_nans(data1, sizeof(data1) / sizeof(double), InterpMethod::Left);
    EXPECT_EQ(count1, 0);
    check_array(data1, {1.0, 2.0, 3.0, 4.0});

    double data2[]{1, NAN, 3, 4, 5};
    const size_t count2 =
        fill_nans(data2, sizeof(data2) / sizeof(double), InterpMethod::Left);
    EXPECT_EQ(count2, 1);
    check_array(data2, {1.0, 1.0, 3.0, 4.0, 5.0});

    double data3[]{1, NAN, 3, NAN, 5};
    const size_t count3 =
        fill_nans(data3, sizeof(data3) / sizeof(double), InterpMethod::Left);
    EXPECT_EQ(count3, 2);
    check_array(data3, {1.0, 1.0, 3.0, 3.0, 5.0});

    double data4[]{1, NAN, NAN, 4, 5};
    const size_t count4 =
        fill_nans(data4, sizeof(data4) / sizeof(double), InterpMethod::Left);
    EXPECT_EQ(count4, 2);
    check_array(data4, {1.0, 1.0, 1.0, 4.0, 5.0});

    double data5[]{1, NAN, NAN, NAN, 5};
    const size_t count5 =
        fill_nans(data5, sizeof(data5) / sizeof(double), InterpMethod::Left);
    EXPECT_EQ(count5, 3);
    check_array(data5, {1.0, 1.0, 1.0, 1.0, 5.0});
}

TEST(fillnans, linear) {
    double data1[]{1, 2, 4, 6, 10};
    const size_t count1 =
        fill_nans(data1, sizeof(data1) / sizeof(double), InterpMethod::Linear);
    EXPECT_EQ(count1, 0);
    check_array(data1, {1.0, 2.0, 4.0, 6.0});

    double data2[]{1, NAN, 4, 6, 10};
    const size_t count2 =
        fill_nans(data2, sizeof(data2) / sizeof(double), InterpMethod::Linear);
    EXPECT_EQ(count2, 1);
    check_array(data2, {1.0, 2.5, 4.0, 6.0, 10.0});

    double data3[]{1, NAN, 4, NAN, 10};
    const size_t count3 =
        fill_nans(data3, sizeof(data3) / sizeof(double), InterpMethod::Linear);
    EXPECT_EQ(count3, 2);
    check_array(data3, {1.0, 2.5, 4.0, 7.0, 10.0});

    double data4[]{1, NAN, NAN, 6, 10};
    const size_t count4 =
        fill_nans(data4, sizeof(data4) / sizeof(double), InterpMethod::Linear);
    EXPECT_EQ(count4, 2);
    check_array(data4, {1.0, 8.0 / 3.0, 13.0 / 3.0, 6.0, 10.0});

    double data5[]{1, NAN, NAN, NAN, 11};
    const size_t count5 =
        fill_nans(data5, sizeof(data5) / sizeof(double), InterpMethod::Linear);
    EXPECT_EQ(count5, 3);
    check_array(data5, {1.0, 3.5, 6.0, 8.5, 11.0});
}
